package ep2;

public class Pedidos extends Veiculos
{
    private float distancia;
    private float peso;
    private float tempo;

    public float getDistancia() 
    {
        return distancia;
    }

    public void setDistancia(float distancia) 
    {
        this.distancia = distancia;
    }

    public float getPeso() 
    {
        return peso;
    }

    public void setPeso(float peso) 
    {
        this.peso = peso;
    }

    public float getTempo() 
    {
        return tempo;
    }

    public void setTempo(float tempo) 
    {
        this.tempo = tempo;
    }
    
    public String mRapido()
    {
        float vDesejada;
        
        vDesejada = this.distancia/this.tempo;
        
        if(this.peso<=50 && vDesejada <= 150)
        {
            return "Moto";
        }
        else if(this.peso>50 && this.peso<=360 && vDesejada<=100)
        {
            return "Carro";
        }
        else if(this.peso>360 && this.peso<=3500 && vDesejada<=80)
        {
            return "Van";
        }
        else if(this.peso>3500 && this.peso<=30000 && vDesejada<=60)
        {
            return "Carreta";
        }
        else
        {
            return "Erro";
        }
    }
    
    public double vLucro(String sLucro)
    {
        double carreta, van, carroG, carroA, motoG, motoA, vDesejada, lcarreta, lvan, lcarroG, lcarroA, lmotoG, lmotoA;
        
        vDesejada = this.distancia/this.tempo;
        
        lcarreta = this.distancia/(8-(this.peso*0.0002));
        carreta = lcarreta * 3.869;
        
        lvan = this.distancia/(10-(this.peso*0.001));
        van = lvan * 3.869;
        
        lcarroG = this.distancia/(14-(this.peso*0.025));
        carroG = lcarroG * 4.449;        
        
        lcarroA = this.distancia/(12-(this.peso*0.0231));
        carroA = lcarroA * 3.499;
        
        lmotoG = this.distancia/(50-(this.peso*0.3));
        motoG = lmotoG * 4.449;
        
        lmotoA = this.distancia/(43-(this.peso*0.4));
        motoA = lmotoA * 3.499;
        
        if ("Carreta".equals(sLucro))
        {
            return carreta;
        }
        else if("Van".equals(sLucro))
        {
            return van;
        }
        else if("CarroG".equals(sLucro))
        {
            return carroG;
        }
        else if("CarroA".equals(sLucro))
        {
            return carroA;
        }
        else if("MotoG".equals(sLucro))
        {
            return motoG;
        }
        else if("MotoA".equals(sLucro))
        {
            return motoA;
        }
        else
        {
            return 0;
        }
    }
    
    public String sLucro()
    {
        double carreta, van, carroG, carroA, motoG, motoA, vDesejada, lcarreta, lvan, lcarroG, lcarroA, lmotoG, lmotoA;
        
        vDesejada = this.distancia/this.tempo;
        
        lcarreta = this.distancia/(8-(this.peso*0.0002));
        carreta = lcarreta * 3.869;
        
        lvan = this.distancia/(10-(this.peso*0.001));
        van = lvan * 3.869;
        
        lcarroG = this.distancia/(14-(this.peso*0.025));
        carroG = lcarroG * 4.449;        
        
        lcarroA = this.distancia/(12-(this.peso*0.0231));
        carroA = lcarroA * 3.499;
        
        lmotoG = this.distancia/(50-(this.peso*0.3));
        motoG = lmotoG * 4.449;
        
        lmotoA = this.distancia/(43-(this.peso*0.4));
        motoA = lmotoA * 3.499;
        
        if(vDesejada<=150)
        {
            if(this.peso<=50 && motoG<motoA && motoG<carroG && motoG<carroA && motoG<van && motoG<carreta)
            {
                return "MotoG";
            }
            else if(this.peso<=50 && motoA<motoG && motoA<carroG && motoA<carroA && motoA<van && motoA<carreta)
            {
                return "MotoA";
            }
            else if(this.peso>50 && this.peso<=360 &&  carroG<motoA && carroG<motoG && carroG<carroA && carroG<van && carroG<carreta)
            {
                return "CarroG";
            }
            else if(this.peso>50 && this.peso<=360 &&  carroA<motoA && carroA<motoG && carroG>carroA && carroA<van && carroA<carreta)
            {
                return "CarroA";
            }
            else if(this.peso>360 && this.peso<=3500 &&  van<motoA && van<motoG && van<carroA && carroG>van && van<carreta)
            {
                return "Van";
            }
            else if(this.peso>3500 && this.peso<=30000 &&  carreta<motoA && carreta<motoG && carreta<carroA && carreta<van && carroG>carreta)
            {
                return "Carreta";
            }
            else
            {
                return "Erro";
            }
        }
        else
        {
            return "Erro";
        }
    }
}