package ep2;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Veiculos 
{
	private String combustivel;
        private String tipo;
	private int numeroCadastro;
        private int numeroPedido;
        private double margemL;
	private float rendimentoG;
	private float rendimentoA;
	private float rendimentoD;
	private float cargaMax;
	private float velocidadeMedia;
	private boolean disponibilidade;

    public int getNumeroPedido() 
    {
        return numeroPedido;
    }

    public void setNumeroPedido(int numeroPedido) 
    {
        this.numeroPedido = numeroPedido;
    }

    public double getMargemL() 
    {
        return margemL;
    }

    public void setMargemL(double margemL) 
    {
        this.margemL = margemL;
    }
	
	public String getCombustivel ()
	{
		return this.combustivel;
	}
	
	public void setCombustivel (String com)
	{
		this.combustivel = com;
	}
	
	public int getNumeroCadastro ()
	{
		return this.numeroCadastro;
	}
	
	public void setNumeroCadastro (int numC)
	{
		this.numeroCadastro = numC;
	}
	
	public float getRendimentoG ()
	{
		return this.rendimentoG;
	}
	
	public void setRendimentoG (float rendG)
	{
		this.rendimentoG = rendG;
	}
	
	public float getRendimentoA ()
	{
		return this.rendimentoA;
	}
	
	public void setRendimentoA (float rendA)
	{
		this.rendimentoA = rendA;
	}
	
	public float getRendimentoD ()
	{
		return this.rendimentoD;
	}
	
	public void setRendimentoD (float rendD)
	{
		this.rendimentoD = rendD;
	}
	
	public float getCargaMax ()
	{
		return this.cargaMax;
	}
	
	public void setCargaMax (float cargM)
	{
		this.cargaMax = cargM;
	}
	
	public float getVelocidadeMedia ()
	{
		return this.velocidadeMedia;
	}
	
	public void setVelocidadeMedia (float velM)
	{
		this.velocidadeMedia = velM;
	}

        public String getTipo() 
        {
            return tipo;
        }

        public void setTipo(String tipo) 
        {
            this.tipo = tipo;
        }

        public boolean getDisponibilidade() 
        {
            return disponibilidade;
        }

        public void setDisponibilidade(boolean disponibilidade) 
        {
            this.disponibilidade = disponibilidade;
        }
        
        public String salvar()
        {
            
            try 
            {
                FileWriter fw = new FileWriter(this.numeroCadastro+".txt");
                PrintWriter pw = new PrintWriter(fw);
                pw.println(this.numeroCadastro);
                pw.println(this.tipo);
                pw.println(this.combustivel);
                if ("Disel".equals(this.combustivel))
                {
                    pw.println(this.rendimentoD);
                }
                else if ("Gasolina".equals(this.combustivel))
                {
                    pw.println(this.rendimentoG);
                }
                else
                {
                    pw.println(this.rendimentoA);
                }
                pw.println(this.cargaMax);
                pw.println(this.velocidadeMedia);
                pw.println(this.disponibilidade);
                
                pw.flush();
                pw.close();
                fw.close();
            } 
            catch (IOException ex) 
            {
                Logger.getLogger(Veiculos.class.getName()).log(Level.SEVERE, null, ex);
            }
            
            return "Alteração feita";
        }
     
        public String salvarLis()
        {
            
            try 
            {
                FileWriter lt = new FileWriter("lista.txt",true);
                PrintWriter plt = new PrintWriter(lt);
                plt.println(this.numeroCadastro);
                
                plt.flush();
                plt.close();
                lt.close();
            } 
            catch (IOException ex) 
            {
                Logger.getLogger(Veiculos.class.getName()).log(Level.SEVERE, null, ex);
            }

            return "Lista salva";
        }
        
        public String salvarP()
        {
            
            try 
            {
                FileWriter fw = new FileWriter("p"+this.numeroPedido+".txt");
                PrintWriter pw = new PrintWriter(fw);
                pw.println(this.numeroCadastro);
                pw.println(this.tipo);
                pw.println(this.combustivel);
                pw.println(this.numeroPedido);
                
                pw.flush();
                pw.close();
                fw.close();
            } 
            catch (IOException ex) 
            {
                Logger.getLogger(Veiculos.class.getName()).log(Level.SEVERE, null, ex);
            }
            
            return "Pedido feito";
        }
     
        public String salvarLisP()
        {
            
            try 
            {
                FileWriter lt = new FileWriter("listaP.txt",true);
                PrintWriter plt = new PrintWriter(lt);
                plt.println(this.numeroPedido);
                
                plt.flush();
                plt.close();
                lt.close();
            } 
            catch (IOException ex) 
            {
                Logger.getLogger(Veiculos.class.getName()).log(Level.SEVERE, null, ex);
            }

            return "Lista de pedidos salva";
        }
        
}
