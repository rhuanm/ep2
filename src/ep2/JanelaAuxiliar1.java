package ep2;

import java.util.ArrayList;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

public class JanelaAuxiliar1 extends javax.swing.JFrame 
{

    private Main receberDaMain;
    
    
    public ArrayList<Pedidos> ListaTemporariaR;
    public ArrayList<Pedidos> ListaTemporariaC;
    public ArrayList<Pedidos> ListaTemporariaB;
    
    public void LoadTableRapido (float dist, float pes, float temp, String tipo, ArrayList<Carreta> ListaCa,  ArrayList<Van> ListaVa,ArrayList<Carro> ListaCr, ArrayList<Moto> ListaMt)
    {
        Object colunas[] = {"Código", "Tipo", "Combustível", "Valor"};
        DefaultTableModel modelo = new DefaultTableModel(colunas, 0);
        Pedidos P = new Pedidos();
        P.setDistancia(dist);
        P.setPeso(pes);
        P.setTempo(temp);
        double valor;
        
        if("Carreta".equals(tipo))
        {
            for(int i=0; i<ListaCa.size();i++)
            {
                valor = P.vLucro("Carreta");
                P.setMargemL(valor);
                P.setTipo(tipo);
                P.setNumeroCadastro(ListaCa.get(i).getNumeroCadastro());
                Object linha[] = new Object[]{ListaCa.get(i).getNumeroCadastro(),ListaCa.get(i).getTipo(),ListaCa.get(i).getCombustivel(),valor};
                modelo.addRow(linha);
                ListaTemporariaR.add(P);
            } 
        }
        else if("Van".equals(tipo))
        {
            for(int i=0; i<ListaVa.size();i++)
            {
                valor = P.vLucro("Van");
                P.setMargemL(valor);
                P.setTipo(tipo);
                P.setNumeroCadastro(ListaVa.get(i).getNumeroCadastro());
                Object linha[] = new Object[]{ListaVa.get(i).getNumeroCadastro(),ListaVa.get(i).getTipo(),ListaVa.get(i).getCombustivel(),valor};
                modelo.addRow(linha);
                ListaTemporariaR.add(P);
            } 
        }
        else if("Carro".equals(tipo))
        {
            for(int i=0; i<ListaCr.size();i++)
            {
                if ("Gasolina".equals(ListaCr.get(i).getCombustivel()))
                {
                    valor = P.vLucro("CarroG");
                    P.setMargemL(valor);
                    P.setTipo(tipo);
                    P.setNumeroCadastro(ListaCr.get(i).getNumeroCadastro());
                }
                else
                {
                    valor = P.vLucro("CarroA");
                    P.setMargemL(valor);
                    P.setTipo(tipo);
                    P.setNumeroCadastro(ListaCr.get(i).getNumeroCadastro());
                }
                Object linha[] = new Object[]{ListaCr.get(i).getNumeroCadastro(),ListaCr.get(i).getTipo(),ListaCr.get(i).getCombustivel(),valor};
                modelo.addRow(linha);
                ListaTemporariaR.add(P);
            } 
        }
        else if("Moto".equals(tipo))
        {
            for(int i=0; i<ListaMt.size();i++)
            {
                if ("Gasolina".equals(ListaMt.get(i).getCombustivel()))
                {
                    valor = P.vLucro("MotoG");
                    P.setMargemL(valor);
                    P.setTipo(tipo);
                    P.setNumeroCadastro(ListaMt.get(i).getNumeroCadastro());
                }
                else
                {
                    valor = P.vLucro("MotoA");
                    P.setMargemL(valor);
                    P.setTipo(tipo);
                    P.setNumeroCadastro(ListaMt.get(i).getNumeroCadastro());
                }
                Object linha[] = new Object[]{ListaMt.get(i).getNumeroCadastro(),ListaMt.get(i).getTipo(),ListaMt.get(i).getCombustivel(),valor};
                modelo.addRow(linha);
                ListaTemporariaR.add(P);
            }  
        }
        
        tbl_jan_rapido.setModel(modelo);
        tbl_jan_rapido.getColumnModel().getColumn(0).setPreferredWidth(50);
        tbl_jan_rapido.getColumnModel().getColumn(1).setPreferredWidth(50);
        tbl_jan_rapido.getColumnModel().getColumn(2).setPreferredWidth(100);
        tbl_jan_rapido.getColumnModel().getColumn(3).setPreferredWidth(50);
    }
    
    public void LoadTableLucro (float dist, float pes, float temp, String tipo, ArrayList<Carreta> ListaCa,  ArrayList<Van> ListaVa,ArrayList<Carro> ListaCr, ArrayList<Moto> ListaMt)
    {
        Object colunas[] = {"Código", "Tipo", "Combustível", "Valor"};
        DefaultTableModel modelo = new DefaultTableModel(colunas, 0);
        Pedidos P = new Pedidos();
        P.setDistancia(dist);
        P.setPeso(pes);
        P.setTempo(temp);
        double valor;
        
        if("Carreta".equals(tipo))
        {
            for(int i=0; i<ListaCa.size();i++)
            {
                valor = P.vLucro("Carreta");
                P.setMargemL(valor);
                P.setTipo(tipo);
                P.setNumeroCadastro(ListaCa.get(i).getNumeroCadastro());
                Object linha[] = new Object[]{ListaCa.get(i).getNumeroCadastro(),ListaCa.get(i).getTipo(),ListaCa.get(i).getCombustivel(),valor};
                modelo.addRow(linha);
                ListaTemporariaC.add(P);
            } 
        }
        else if("Van".equals(tipo))
        {
            for(int i=0; i<ListaVa.size();i++)
            {
                valor = P.vLucro("Van");
                P.setMargemL(valor);
                P.setTipo(tipo);
                P.setNumeroCadastro(ListaVa.get(i).getNumeroCadastro());
                Object linha[] = new Object[]{ListaVa.get(i).getNumeroCadastro(),ListaVa.get(i).getTipo(),ListaVa.get(i).getCombustivel(),valor};
                modelo.addRow(linha);
                ListaTemporariaC.add(P);
            } 
        }
        else if("CarroG".equals(tipo))
        {
            for(int i=0; i<ListaCr.size();i++)
            {
                valor = P.vLucro("CarroG");
                P.setMargemL(valor);
                P.setTipo(tipo);
                P.setNumeroCadastro(ListaCr.get(i).getNumeroCadastro());
                Object linha[] = new Object[]{ListaCr.get(i).getNumeroCadastro(),ListaVa.get(i).getTipo(),ListaVa.get(i).getCombustivel(),valor};
                modelo.addRow(linha);
                ListaTemporariaC.add(P);
            } 
        }
        else if("CarroA".equals(tipo))
        {
            for(int i=0; i<ListaCr.size();i++)
            {
                valor = P.vLucro("CarroA");
                P.setMargemL(valor);
                P.setTipo(tipo);
                P.setNumeroCadastro(ListaCr.get(i).getNumeroCadastro());
                Object linha[] = new Object[]{ListaCr.get(i).getNumeroCadastro(),ListaVa.get(i).getTipo(),ListaVa.get(i).getCombustivel(),valor};
                modelo.addRow(linha);
                ListaTemporariaC.add(P);
            } 
        }
        else if("MotoG".equals(tipo))
        {
            for(int i=0; i<ListaMt.size();i++)
            {
                valor = P.vLucro("MotoG");
                P.setMargemL(valor);
                P.setTipo(tipo);
                P.setNumeroCadastro(ListaMt.get(i).getNumeroCadastro());
                Object linha[] = new Object[]{ListaMt.get(i).getNumeroCadastro(),ListaVa.get(i).getTipo(),ListaVa.get(i).getCombustivel(),valor};
                modelo.addRow(linha);
                ListaTemporariaC.add(P);
            } 
        }
        else if("MotoA".equals(tipo))
        {
            for(int i=0; i<ListaMt.size();i++)
            {
                valor = P.vLucro("MotoA");
                P.setMargemL(valor);
                P.setTipo(tipo);
                P.setNumeroCadastro(ListaMt.get(i).getNumeroCadastro());
                Object linha[] = new Object[]{ListaMt.get(i).getNumeroCadastro(),ListaVa.get(i).getTipo(),ListaVa.get(i).getCombustivel(),valor};
                modelo.addRow(linha);
                ListaTemporariaC.add(P);
            } 
        }
        
        tbl_jan_custo.setModel(modelo);
        tbl_jan_custo.getColumnModel().getColumn(0).setPreferredWidth(50);
        tbl_jan_custo.getColumnModel().getColumn(1).setPreferredWidth(50);
        tbl_jan_custo.getColumnModel().getColumn(2).setPreferredWidth(100);
        tbl_jan_custo.getColumnModel().getColumn(3).setPreferredWidth(50); 
    }
    /**
     * Creates new form JanelaAuxiliar1
     */
    public JanelaAuxiliar1() 
    {
        initComponents();
        ListaTemporariaR = new ArrayList();
        ListaTemporariaC = new ArrayList();
        btn_jan_rapido.setEnabled(false);
        btn_jan_custo.setEnabled(false);
        btn_jan_beneficio.setEnabled(false);
        ct_jan_rapido.setEnabled(false);
        ct_jan_custo.setEnabled(false);
        ct_jan_beneficio.setEnabled(false);
        
    }
    
    public void enviaListatbl1(Main veioDaMain, float dist, float pes, float temp, String tipo, ArrayList<Carro> ListaCr, ArrayList<Moto> ListaMt, ArrayList<Carreta> ListaCa, ArrayList<Van> ListaVa)
    {
        LoadTableRapido(dist, pes, temp, tipo,ListaCa,ListaVa,ListaCr,ListaMt);
    }
    
    public void enviaListatbl2(Main veioDaMain, float dist, float pes, float temp, String tipo, ArrayList<Carro> ListaCr, ArrayList<Moto> ListaMt, ArrayList<Carreta> ListaCa, ArrayList<Van> ListaVa)
    {
        LoadTableLucro(dist, pes, temp, tipo,ListaCa,ListaVa,ListaCr,ListaMt);
    }
     
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">                          
    private void initComponents() {

        jScrollPane2 = new javax.swing.JScrollPane();
        jTable2 = new javax.swing.JTable();
        jScrollPane4 = new javax.swing.JScrollPane();
        jTable4 = new javax.swing.JTable();
        jScrollPane5 = new javax.swing.JScrollPane();
        jTable5 = new javax.swing.JTable();
        jScrollPane6 = new javax.swing.JScrollPane();
        jTable6 = new javax.swing.JTable();
        jPanel1 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tbl_jan_rapido = new javax.swing.JTable();
        jScrollPane3 = new javax.swing.JScrollPane();
        tbl_jan_custo = new javax.swing.JTable();
        jScrollPane7 = new javax.swing.JScrollPane();
        tbl_jan_beneficio = new javax.swing.JTable();
        jLabel1 = new javax.swing.JLabel();
        ct_jan_rapido = new javax.swing.JTextField();
        btn_jan_rapido = new javax.swing.JToggleButton();
        jLabel2 = new javax.swing.JLabel();
        ct_jan_custo = new javax.swing.JTextField();
        btn_jan_custo = new javax.swing.JToggleButton();
        jLabel3 = new javax.swing.JLabel();
        ct_jan_beneficio = new javax.swing.JTextField();
        btn_jan_beneficio = new javax.swing.JToggleButton();
        ct_jan_Rtipo = new javax.swing.JTextField();
        ct_jan_Ctipo = new javax.swing.JTextField();
        ct_jan_Rvalor = new javax.swing.JTextField();
        ct_jan_Cvalor = new javax.swing.JTextField();
        ct_jan_Rnumero = new javax.swing.JTextField();
        ct_jan_Cnumero = new javax.swing.JTextField();
        ct_jan_Btipo = new javax.swing.JTextField();
        ct_jan_Bvalor = new javax.swing.JTextField();
        ct_jan_Bnumero = new javax.swing.JTextField();

        jTable2.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane2.setViewportView(jTable2);

        jTable4.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane4.setViewportView(jTable4);

        jTable5.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane5.setViewportView(jTable5);

        jTable6.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane6.setViewportView(jTable6);

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        tbl_jan_rapido.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Número", "Tipo", "Combustível", "Preço"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Integer.class, java.lang.String.class, java.lang.String.class, java.lang.Float.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tbl_jan_rapido.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tbl_jan_rapidoMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tbl_jan_rapido);
        if (tbl_jan_rapido.getColumnModel().getColumnCount() > 0) {
            tbl_jan_rapido.getColumnModel().getColumn(0).setPreferredWidth(50);
            tbl_jan_rapido.getColumnModel().getColumn(1).setPreferredWidth(50);
            tbl_jan_rapido.getColumnModel().getColumn(2).setPreferredWidth(100);
            tbl_jan_rapido.getColumnModel().getColumn(3).setPreferredWidth(50);
        }

        tbl_jan_custo.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Número", "Tipo", "Combustìvel", "Preço"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Integer.class, java.lang.String.class, java.lang.String.class, java.lang.Float.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tbl_jan_custo.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tbl_jan_custoMouseClicked(evt);
            }
        });
        jScrollPane3.setViewportView(tbl_jan_custo);
        if (tbl_jan_custo.getColumnModel().getColumnCount() > 0) {
            tbl_jan_custo.getColumnModel().getColumn(0).setPreferredWidth(50);
            tbl_jan_custo.getColumnModel().getColumn(1).setPreferredWidth(50);
            tbl_jan_custo.getColumnModel().getColumn(2).setPreferredWidth(100);
            tbl_jan_custo.getColumnModel().getColumn(3).setPreferredWidth(50);
        }

        tbl_jan_beneficio.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Número", "Tipo", "Combustível", "Preço"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Integer.class, java.lang.String.class, java.lang.String.class, java.lang.Float.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, true, true
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane7.setViewportView(tbl_jan_beneficio);
        if (tbl_jan_beneficio.getColumnModel().getColumnCount() > 0) {
            tbl_jan_beneficio.getColumnModel().getColumn(0).setPreferredWidth(50);
            tbl_jan_beneficio.getColumnModel().getColumn(1).setPreferredWidth(50);
            tbl_jan_beneficio.getColumnModel().getColumn(2).setPreferredWidth(100);
            tbl_jan_beneficio.getColumnModel().getColumn(3).setPreferredWidth(50);
        }

        jLabel1.setText("+ Rápido");

        btn_jan_rapido.setText("Escolher");
        btn_jan_rapido.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_jan_rapidoActionPerformed(evt);
            }
        });

        jLabel2.setText("Custo");

        btn_jan_custo.setText("Escolher");
        btn_jan_custo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_jan_custoActionPerformed(evt);
            }
        });

        jLabel3.setText("Custo Benifício");

        btn_jan_beneficio.setText("Escolher");
        btn_jan_beneficio.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_jan_beneficioActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 375, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel1)
                            .addComponent(ct_jan_rapido, javax.swing.GroupLayout.PREFERRED_SIZE, 125, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btn_jan_rapido)))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 375, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(ct_jan_custo)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel2)
                                    .addComponent(btn_jan_custo))
                                .addGap(0, 0, Short.MAX_VALUE))))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jScrollPane7, javax.swing.GroupLayout.PREFERRED_SIZE, 375, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(ct_jan_beneficio)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel3)
                                    .addComponent(btn_jan_beneficio))
                                .addGap(0, 0, Short.MAX_VALUE)))))
                .addGap(0, 32, Short.MAX_VALUE))
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(ct_jan_Rtipo, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(ct_jan_Rvalor, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(ct_jan_Rnumero, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(ct_jan_Ctipo, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(ct_jan_Cvalor, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(ct_jan_Cnumero, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(264, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addGap(18, 18, 18)
                        .addComponent(ct_jan_rapido, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(16, 16, 16)
                        .addComponent(btn_jan_rapido))
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(ct_jan_Rtipo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(ct_jan_Rvalor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(ct_jan_Rnumero, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 92, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(ct_jan_custo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btn_jan_custo)))
                .addGap(2, 2, 2)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(ct_jan_Ctipo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(ct_jan_Cvalor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(ct_jan_Cnumero, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(10, 10, 10)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jScrollPane7, javax.swing.GroupLayout.PREFERRED_SIZE, 91, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel3)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(ct_jan_beneficio, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btn_jan_beneficio)))
                .addGap(271, 271, 271))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(ct_jan_Btipo, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(ct_jan_Bvalor, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(ct_jan_Bnumero, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 359, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(ct_jan_Btipo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(ct_jan_Bvalor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(ct_jan_Bnumero, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>                        

    private void btn_jan_rapidoActionPerformed(java.awt.event.ActionEvent evt) {                                               
        int index = tbl_jan_rapido.getSelectedRow();
        
        int cod = Integer.parseInt(ct_jan_Rnumero.getText());
        String tp = ct_jan_Rtipo.getText();
        double com = Double.parseDouble(ct_jan_Rvalor.getText());
        int num = Integer.parseInt(ct_jan_rapido.getText());
        
        
        Pedidos P = new Pedidos();
        P.setNumeroCadastro(cod);
        P.setTipo(tp);
        P.setMargemL(com);
        P.setNumeroPedido(num);
        
        JOptionPane.showMessageDialog(null,P.salvarP());
        JOptionPane.showMessageDialog(null,P.salvarLisP());
        
        this.dispose();
    }                                              

    private void btn_jan_custoActionPerformed(java.awt.event.ActionEvent evt) {                                              
        int index = tbl_jan_custo.getSelectedRow();
        
        int cod = Integer.parseInt(ct_jan_Cnumero.getText());
        String tp = ct_jan_Ctipo.getText();
        double com = Double.parseDouble(ct_jan_Cvalor.getText());
        int num = Integer.parseInt(ct_jan_custo.getText());
        
        
        Pedidos P = new Pedidos();
        P.setNumeroCadastro(cod);
        P.setTipo(tp);
        P.setMargemL(com);
        P.setNumeroPedido(num);
        
        JOptionPane.showMessageDialog(null,P.salvarP());
        JOptionPane.showMessageDialog(null,P.salvarLisP());
        
        this.dispose();
    }                                             

    private void btn_jan_beneficioActionPerformed(java.awt.event.ActionEvent evt) {                                                  
        this.dispose();
    }                                                 

    private void tbl_jan_rapidoMouseClicked(java.awt.event.MouseEvent evt) {                                            
        btn_jan_rapido.setEnabled(true);
        ct_jan_rapido.setEnabled(true);
        
        int index = tbl_jan_rapido.getSelectedRow();
        
        if(index>=0 && index<ListaTemporariaR.size())
        { 
            Pedidos P = ListaTemporariaR.get(index);
            
            ct_jan_Rtipo.setText(P.getTipo());
            ct_jan_Rvalor.setText(String.valueOf(P.getMargemL()));
            ct_jan_Rnumero.setText(String.valueOf(P.getNumeroCadastro()));

        }
        
    }                                           

    private void tbl_jan_custoMouseClicked(java.awt.event.MouseEvent evt) {                                           
        btn_jan_custo.setEnabled(true);
        ct_jan_custo.setEnabled(true);
        
        int index = tbl_jan_custo.getSelectedRow();
        
        if(index>=0 && index<ListaTemporariaC.size())
        { 
            Pedidos P = ListaTemporariaC.get(index);
            
            ct_jan_Ctipo.setText(P.getTipo());
            ct_jan_Cvalor.setText(String.valueOf(P.getMargemL()));
            ct_jan_Cnumero.setText(String.valueOf(P.getNumeroCadastro()));

        }
    }                                          

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(JanelaAuxiliar1.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(JanelaAuxiliar1.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(JanelaAuxiliar1.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(JanelaAuxiliar1.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new JanelaAuxiliar1().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify                     
    private javax.swing.JToggleButton btn_jan_beneficio;
    private javax.swing.JToggleButton btn_jan_custo;
    private javax.swing.JToggleButton btn_jan_rapido;
    private javax.swing.JTextField ct_jan_Bnumero;
    private javax.swing.JTextField ct_jan_Btipo;
    private javax.swing.JTextField ct_jan_Bvalor;
    private javax.swing.JTextField ct_jan_Cnumero;
    private javax.swing.JTextField ct_jan_Ctipo;
    private javax.swing.JTextField ct_jan_Cvalor;
    private javax.swing.JTextField ct_jan_Rnumero;
    private javax.swing.JTextField ct_jan_Rtipo;
    private javax.swing.JTextField ct_jan_Rvalor;
    private javax.swing.JTextField ct_jan_beneficio;
    private javax.swing.JTextField ct_jan_custo;
    private javax.swing.JTextField ct_jan_rapido;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JScrollPane jScrollPane6;
    private javax.swing.JScrollPane jScrollPane7;
    private javax.swing.JTable jTable2;
    private javax.swing.JTable jTable4;
    private javax.swing.JTable jTable5;
    private javax.swing.JTable jTable6;
    private javax.swing.JTable tbl_jan_beneficio;
    private javax.swing.JTable tbl_jan_custo;
    private javax.swing.JTable tbl_jan_rapido;
    // End of variables declaration                   
}
