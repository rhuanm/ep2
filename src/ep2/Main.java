package ep2;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

public class Main extends javax.swing.JFrame 
{

    private JanelaAuxiliar1 janela2;
    
    ArrayList<Veiculos> ListaVei;
    ArrayList<Pedidos> ListaPed;
    ArrayList<Carreta> ListaCarreta;
    ArrayList<Van> ListaVan;
    ArrayList<Carro> ListaCarro;
    ArrayList<Carro> ListaCarroG;
    ArrayList<Carro> ListaCarroA;
    ArrayList<Moto> ListaMotoG;
    ArrayList<Moto> ListaMotoA;
    ArrayList<Moto> ListaMoto;
    
    String modo;   
    
    public void LoadTableReg ()
    {
        Object colunas[] = {"Código", "Tipo", "Combustível", "Disponivel"};
        DefaultTableModel modelo = new DefaultTableModel(colunas, 0); 
        
        for(int i=0; i<ListaVei.size();i++)
        {
            Object linha[] = new Object[]{ListaVei.get(i).getNumeroCadastro(),ListaVei.get(i).getTipo(),ListaVei.get(i).getCombustivel(),ListaVei.get(i).getDisponibilidade()};
            modelo.addRow(linha);
        }
        
        tbl_reg.setModel(modelo);
        tbl_reg.getColumnModel().getColumn(1).setPreferredWidth(100);
        tbl_reg.getColumnModel().getColumn(2).setPreferredWidth(100);
        tbl_reg.getColumnModel().getColumn(3).setPreferredWidth(100);
    }
    
    public void LoadTablePed()
    {
        Object colunas[] = {"Código", "Tipo", "Valor", "Numero"};
        DefaultTableModel modelo = new DefaultTableModel(colunas, 0); 
        
        for(int i=0; i<ListaPed.size();i++)
        {
            Object linha[] = new Object[]{ListaPed.get(i).getNumeroCadastro(),ListaPed.get(i).getTipo(),ListaPed.get(i).getMargemL(),ListaPed.get(i).getNumeroPedido()};
            modelo.addRow(linha);
        }
        tbl_esc.setModel(modelo);
        tbl_esc.getColumnModel().getColumn(1).setPreferredWidth(100);
        tbl_esc.getColumnModel().getColumn(2).setPreferredWidth(100);
        tbl_esc.getColumnModel().getColumn(3).setPreferredWidth(100);
    }
    
    public void LerArq()
    {
        try 
        {
            BufferedReader arqList = new BufferedReader(new FileReader("lista.txt"));
            String linha = "";
            
            try 
            {
                linha = arqList.readLine();
                while(linha != null)
                {   
                    BufferedReader arqCont = new BufferedReader(new FileReader(linha+".txt"));
                    String cont1, cont2, cont3, cont4, cont5, cont6, cont7;
                    cont1 = arqCont.readLine();
                    cont2 = arqCont.readLine();
                    cont3 = arqCont.readLine();
                    cont4 = arqCont.readLine();
                    cont5 = arqCont.readLine();
                    cont6 = arqCont.readLine();
                    cont7 = arqCont.readLine();
                    
                    
                    BufferedReader arqDados = new BufferedReader(new FileReader(linha+".txt"));
                    if("Carreta".equals(cont2))
                    {
                        Carreta CA = new Carreta();
                        CA.setNumeroCadastro(Integer.parseInt(arqDados.readLine()));
                        CA.setTipo(arqDados.readLine());
                        CA.setCombustivel(arqDados.readLine());
                        CA.setRendimentoD(Float.parseFloat(arqDados.readLine()));
                        CA.setCargaMax(Float.parseFloat(arqDados.readLine()));
                        CA.setVelocidadeMedia(Float.parseFloat(arqDados.readLine()));
                        CA.setDisponibilidade(Boolean.parseBoolean(arqDados.readLine()));
                        ListaVei.add(CA);
                        if("true".equals(cont7))
                        {
                           ListaCarreta.add(CA); 
                        }
                        LoadTableReg();
                        linha = arqList.readLine();
                        arqDados.close();
                        arqCont.close();
                    }
                    
                    else if("Van".equals(cont2))
                    {
                        Van VA = new Van();
                        VA.setNumeroCadastro(Integer.parseInt(arqDados.readLine()));
                        VA.setTipo(arqDados.readLine());
                        VA.setCombustivel(arqDados.readLine());
                        VA.setRendimentoD(Float.parseFloat(arqDados.readLine()));
                        VA.setCargaMax(Float.parseFloat(arqDados.readLine()));
                        VA.setVelocidadeMedia(Float.parseFloat(arqDados.readLine()));
                        VA.setDisponibilidade(Boolean.parseBoolean(arqDados.readLine()));
                        ListaVei.add(VA);
                        if("true".equals(cont7))
                        {
                           ListaVan.add(VA); 
                        }
                        LoadTableReg(); 
                        linha = arqList.readLine();
                        arqDados.close();
                        arqCont.close();
                    }
                    
                    else if("Carro".equals(cont2))
                    {
                        Carro CR = new Carro();
                        CR.setNumeroCadastro(Integer.parseInt(arqDados.readLine()));
                        CR.setTipo(arqDados.readLine());
                        CR.setCombustivel(arqDados.readLine());
                        if ("Gasolina".equals(cont3))
                        {
                            CR.setRendimentoG(Float.parseFloat(arqDados.readLine()));
                        }
                        else
                        {
                            CR.setRendimentoA(Float.parseFloat(arqDados.readLine()));
                        }
                        CR.setCargaMax(Float.parseFloat(arqDados.readLine()));
                        CR.setVelocidadeMedia(Float.parseFloat(arqDados.readLine()));
                        CR.setDisponibilidade(Boolean.parseBoolean(arqDados.readLine()));
                        ListaVei.add(CR);
                        ListaCarro.add(CR);
                        if("true".equals(cont7))
                        {
                           if("Gasolina".equals(cont3))
                           {
                              ListaCarroG.add(CR);
                           }
                           else
                           {
                              ListaCarroA.add(CR); 
                           }
                        }
                        LoadTableReg(); 
                        linha = arqList.readLine();
                        arqDados.close();
                        arqCont.close();
                    }
                    
                    else if("Moto".equals(cont2))
                    {
                        Moto MT = new Moto();
                        MT.setNumeroCadastro(Integer.parseInt(arqDados.readLine()));
                        MT.setTipo(arqDados.readLine());
                        MT.setCombustivel(arqDados.readLine());
                        if ("Gasolina".equals(cont3))
                        {
                            MT.setRendimentoG(Float.parseFloat(arqDados.readLine()));
                        }
                        else
                        {
                            MT.setRendimentoA(Float.parseFloat(arqDados.readLine()));
                        }
                        MT.setCargaMax(Float.parseFloat(arqDados.readLine()));
                        MT.setVelocidadeMedia(Float.parseFloat(arqDados.readLine()));
                        MT.setDisponibilidade(Boolean.parseBoolean(arqDados.readLine()));
                        ListaVei.add(MT);
                        ListaMoto.add(MT);
                        if("true".equals(cont7))
                        {
                           if("Gasolina".equals(cont3))
                           {
                              ListaMotoG.add(MT);
                           }
                           else
                           {
                              ListaMotoA.add(MT); 
                           }
                        }
                        LoadTableReg(); 
                        linha = arqList.readLine();
                        arqDados.close();
                        arqCont.close();
                    }
                    
                }
             arqList.close();
            } 
            catch (IOException ex) 
            {
                Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
            }
            
        } 
        catch (FileNotFoundException ex) 
        {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
    public void LerArqP()
    {
        try 
        {
            BufferedReader arqList = new BufferedReader(new FileReader("listaP.txt"));
            String linha = "";
            
            try 
            {
                linha = arqList.readLine();
                while(linha != null)
                {   
                    BufferedReader arqDados = new BufferedReader(new FileReader("p"+linha+".txt"));
                    Pedidos P = new Pedidos();
                    
                    P.setNumeroCadastro(Integer.parseInt(arqDados.readLine()));
                    P.setTipo(arqDados.readLine());
                    P.setMargemL(Double.parseDouble(arqDados.readLine()));
                    P.setNumeroPedido(Integer.parseInt(arqDados.readLine()));
                    
                    ListaPed.add(P);
                    LoadTablePed();
                    linha = arqList.readLine();
                    arqDados.close();
                }
             arqList.close();
            } 
            catch (IOException ex) 
            {
                Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
            }
            
        } 
        catch (FileNotFoundException ex) 
        {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
    public void ExcluirArq(String num)
    {
        try 
        {
            File arq = new File(num+".txt");
            BufferedReader BR = new BufferedReader(new FileReader("lista.txt"));
            String linha = BR.readLine();
            ArrayList<String> salvarL = new ArrayList();
            
            while(linha != null)
            {
                if(linha.equals(num) == false)
                {
                   salvarL.add(linha);
                }
                linha = BR.readLine();
            }
            
            BR.close();
            FileWriter FW = new FileWriter("lista.txt", true);
            FW.close();            
            
            BufferedWriter BW = new BufferedWriter(new FileWriter("lista.txt"));
            
            for(int i=0; i<salvarL.size(); i++)
            {
                BW.write(salvarL.get(i));
                BW.newLine();
            }
            
            BW.flush();
            BW.close();
            arq.delete();

        } 
        catch (IOException ex) 
        {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void ExcluirArqP(String num)
    {
        try 
        {
            File arq = new File("p"+num+".txt");
            BufferedReader BR = new BufferedReader(new FileReader("listaP.txt"));
            String linha = BR.readLine();
            ArrayList<String> salvarL = new ArrayList();
            
            while(linha != null)
            {
                if(linha.equals(num) == false)
                {
                   salvarL.add(linha);
                }
                linha = BR.readLine();
            }
            
            BR.close();
            FileWriter FW = new FileWriter("listaP.txt", true);
            FW.close();            
            
            BufferedWriter BW = new BufferedWriter(new FileWriter("listaP.txt"));
            
            for(int i=0; i<salvarL.size(); i++)
            {
                BW.write(salvarL.get(i));
                BW.newLine();
            }
            
            BW.flush();
            BW.close();
            arq.delete();

        } 
        catch (IOException ex) 
        {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public Main() 
    {
        initComponents();
        ListaPed = new ArrayList();
        ListaVei = new ArrayList();
        ListaCarreta = new ArrayList();
        ListaVan = new ArrayList();
        ListaCarroG = new ArrayList();
        ListaCarroA = new ArrayList();
        ListaCarro = new ArrayList();
        ListaMotoG = new ArrayList();
        ListaMotoA = new ArrayList();
        ListaMoto = new ArrayList();
        setLocationRelativeTo(null);
        modo = "Navegar";
        
        this.janela2 = new JanelaAuxiliar1();
        
        ManipulaInterface();
        LerArq();
        LerArqP();
        
    }  

    public void ManipulaInterface()
    {
        switch(modo)
        {
            case "Navegar":
                btn_reg_salvar.setEnabled(false);
                btn_reg_cancelar.setEnabled(false);
                btn_reg_editar.setEnabled(true);
                btn_reg_excluir.setEnabled(true);
                btn_reg_novo.setEnabled(true);
                btn_esc_novo.setEnabled(true);
                btn_esc_excluir.setEnabled(true);
                btn_esc_verificar.setEnabled(false);
                ct_reg_codigo.setEditable(false);
                ct_reg_tipo.setEditable(false);
                ct_reg_combustivel.setEditable(false);
                ct_reg_codigo.setText("");
                ct_reg_combustivel.setText("");
                ct_reg_tipo.setText("");
                
                btn_esc_novo.setEnabled(true);
                btn_esc_excluir.setEnabled(true);
                btn_esc_verificar.setEnabled(false);
                ct_esc_distancia.setEditable(false);
                ct_esc_lucro.setEditable(false);
                ct_esc_peso.setEditable(false);
                ct_esc_tempo.setEditable(false);
                ct_esc_distancia.setText("");
                ct_esc_lucro.setText("");
                ct_esc_peso.setText("");
                ct_esc_tempo.setText("");
                break;
                
            case "Novo":
                btn_reg_salvar.setEnabled(true);
                btn_reg_cancelar.setEnabled(true);
                btn_reg_editar.setEnabled(false);
                btn_reg_excluir.setEnabled(false);
                btn_reg_novo.setEnabled(false);
                ct_reg_codigo.setEditable(true);
                ct_reg_tipo.setEditable(true);
                ct_reg_combustivel.setEditable(true);
                
                btn_esc_novo.setEnabled(false);
                btn_esc_excluir.setEnabled(false);
                btn_esc_verificar.setEnabled(true);
                ct_esc_distancia.setEditable(true);
                ct_esc_lucro.setEditable(true);
                ct_esc_peso.setEditable(true);
                ct_esc_tempo.setEditable(true);
                break;
                
            case "Editar":
                btn_reg_salvar.setEnabled(true);
                btn_reg_cancelar.setEnabled(true);
                btn_reg_editar.setEnabled(false);
                btn_reg_excluir.setEnabled(false);
                btn_reg_novo.setEnabled(false);
                ct_reg_codigo.setEditable(true);
                ct_reg_tipo.setEditable(true);
                ct_reg_combustivel.setEditable(true);
                
                btn_esc_novo.setEnabled(false);
                btn_esc_excluir.setEnabled(false);
                btn_esc_verificar.setEnabled(true);
                ct_esc_distancia.setEditable(true);
                ct_esc_lucro.setEditable(true);
                ct_esc_peso.setEditable(true);
                ct_esc_tempo.setEditable(true);
                break;
                
            case "Excluir":
                btn_reg_salvar.setEnabled(false);
                btn_reg_cancelar.setEnabled(false);
                btn_reg_editar.setEnabled(true);
                btn_reg_excluir.setEnabled(true);
                btn_reg_novo.setEnabled(true);
                ct_reg_codigo.setEditable(false);
                ct_reg_tipo.setEditable(false);
                ct_reg_combustivel.setEditable(false);
                
                btn_esc_novo.setEnabled(false);
                btn_esc_excluir.setEnabled(false);
                btn_esc_verificar.setEnabled(true);
                ct_esc_distancia.setEditable(true);
                ct_esc_lucro.setEditable(true);
                ct_esc_peso.setEditable(true);
                ct_esc_tempo.setEditable(true);
                break;
                
            case "Selecao":
                btn_reg_salvar.setEnabled(false);
                btn_reg_cancelar.setEnabled(false);
                btn_reg_editar.setEnabled(true);
                btn_reg_excluir.setEnabled(true);
                btn_reg_novo.setEnabled(true);
                ct_reg_codigo.setEditable(false);
                ct_reg_tipo.setEditable(false);
                ct_reg_combustivel.setEditable(false);
                
                btn_esc_novo.setEnabled(true);
                btn_esc_excluir.setEnabled(true);
                btn_esc_verificar.setEnabled(false);
                ct_esc_distancia.setEditable(false);
                ct_esc_lucro.setEditable(false);
                ct_esc_peso.setEditable(false);
                ct_esc_tempo.setEditable(false);
                break;    
                
            default: System.out.println("Modo Inválido");
        }
    }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">                          
    private void initComponents() {

        jTabbedPane2 = new javax.swing.JTabbedPane();
        jPanel1 = new javax.swing.JPanel();
        jPanel3 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        ct_reg_codigo = new javax.swing.JTextField();
        ct_reg_tipo = new javax.swing.JTextField();
        ct_reg_combustivel = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        ce_reg_status = new javax.swing.JCheckBox();
        btn_reg_salvar = new javax.swing.JButton();
        btn_reg_cancelar = new javax.swing.JButton();
        btn_reg_novo = new javax.swing.JButton();
        btn_reg_editar = new javax.swing.JButton();
        btn_reg_excluir = new javax.swing.JToggleButton();
        jScrollPane3 = new javax.swing.JScrollPane();
        tbl_reg = new javax.swing.JTable();
        jPanel2 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        tbl_esc = new javax.swing.JTable();
        jPanel4 = new javax.swing.JPanel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        btn_esc_verificar = new javax.swing.JButton();
        ct_esc_peso = new javax.swing.JTextField();
        ct_esc_tempo = new javax.swing.JTextField();
        ct_esc_lucro = new javax.swing.JTextField();
        ct_esc_distancia = new javax.swing.JTextField();
        btn_esc_novo = new javax.swing.JButton();
        btn_esc_excluir = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder("Dados"));

        jLabel1.setText("Código");

        jLabel2.setText("Tipo");

        jLabel3.setText("Combustível");

        ct_reg_codigo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ct_reg_codigoActionPerformed(evt);
            }
        });

        ct_reg_tipo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ct_reg_tipoActionPerformed(evt);
            }
        });

        ct_reg_combustivel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ct_reg_combustivelActionPerformed(evt);
            }
        });

        jLabel4.setText("Status");

        ce_reg_status.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ce_reg_statusActionPerformed(evt);
            }
        });

        btn_reg_salvar.setText("Salvar");
        btn_reg_salvar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_reg_salvarActionPerformed(evt);
            }
        });

        btn_reg_cancelar.setText("Cancelar");
        btn_reg_cancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_reg_cancelarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(jLabel3)
                        .addGap(24, 24, 24)
                        .addComponent(ct_reg_combustivel))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(jLabel2)
                        .addGap(63, 63, 63)
                        .addComponent(ct_reg_tipo))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addGap(50, 50, 50)
                        .addComponent(ct_reg_codigo, javax.swing.GroupLayout.PREFERRED_SIZE, 163, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(38, 38, 38)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel4)
                    .addComponent(btn_reg_salvar))
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGap(54, 54, 54)
                        .addComponent(ce_reg_status)
                        .addGap(64, 64, 64))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btn_reg_cancelar)
                        .addGap(27, 27, 27))))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(ce_reg_status, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel1)
                        .addComponent(ct_reg_codigo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel4)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(ct_reg_tipo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(ct_reg_combustivel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btn_reg_salvar)
                    .addComponent(btn_reg_cancelar))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        btn_reg_novo.setText("Novo");
        btn_reg_novo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_reg_novoActionPerformed(evt);
            }
        });

        btn_reg_editar.setText("Editar");
        btn_reg_editar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_reg_editarActionPerformed(evt);
            }
        });

        btn_reg_excluir.setText("excluir");
        btn_reg_excluir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_reg_excluirActionPerformed(evt);
            }
        });

        tbl_reg.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Código", "Tipo", "Combustível", "Disponível"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Integer.class, java.lang.String.class, java.lang.String.class, java.lang.Boolean.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tbl_reg.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tbl_regMouseClicked(evt);
            }
        });
        jScrollPane3.setViewportView(tbl_reg);
        if (tbl_reg.getColumnModel().getColumnCount() > 0) {
            tbl_reg.getColumnModel().getColumn(1).setPreferredWidth(100);
            tbl_reg.getColumnModel().getColumn(2).setPreferredWidth(100);
            tbl_reg.getColumnModel().getColumn(3).setPreferredWidth(100);
        }

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(btn_reg_novo)
                .addGap(128, 128, 128)
                .addComponent(btn_reg_excluir)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btn_reg_editar)
                .addContainerGap())
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(jScrollPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 509, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap(153, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btn_reg_novo)
                    .addComponent(btn_reg_editar)
                    .addComponent(btn_reg_excluir))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel1Layout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 125, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(175, Short.MAX_VALUE)))
        );

        jTabbedPane2.addTab("Registro", jPanel1);

        tbl_esc.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Código", "Tipo", "Valor", "Numero"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Integer.class, java.lang.String.class, java.lang.String.class, java.lang.Integer.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tbl_esc.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tbl_escMouseClicked(evt);
            }
        });
        jScrollPane2.setViewportView(tbl_esc);
        if (tbl_esc.getColumnModel().getColumnCount() > 0) {
            tbl_esc.getColumnModel().getColumn(1).setPreferredWidth(100);
            tbl_esc.getColumnModel().getColumn(2).setPreferredWidth(100);
            tbl_esc.getColumnModel().getColumn(3).setPreferredWidth(100);
        }

        jPanel4.setBorder(javax.swing.BorderFactory.createTitledBorder("Pedido"));

        jLabel5.setText("Distância");

        jLabel6.setText("Peso");

        jLabel7.setText("Tempo");

        jLabel8.setText("Margem de Lucro");

        btn_esc_verificar.setText("Verificar");
        btn_esc_verificar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_esc_verificarActionPerformed(evt);
            }
        });

        ct_esc_distancia.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ct_esc_distanciaActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGap(26, 26, 26)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel6)
                    .addComponent(jLabel7)
                    .addComponent(jLabel5))
                .addGap(45, 45, 45)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(ct_esc_peso, javax.swing.GroupLayout.DEFAULT_SIZE, 97, Short.MAX_VALUE)
                    .addComponent(ct_esc_tempo)
                    .addComponent(ct_esc_distancia))
                .addGap(14, 14, 14)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                        .addComponent(jLabel8)
                        .addGap(18, 18, 18)
                        .addComponent(ct_esc_lucro, javax.swing.GroupLayout.PREFERRED_SIZE, 103, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(btn_esc_verificar, javax.swing.GroupLayout.Alignment.TRAILING))
                .addGap(46, 46, 46))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(jLabel8)
                    .addComponent(ct_esc_lucro, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(ct_esc_distancia, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, Short.MAX_VALUE)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel6)
                            .addComponent(btn_esc_verificar))
                        .addGap(12, 12, 12))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                        .addComponent(ct_esc_peso, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)))
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel7)
                    .addComponent(ct_esc_tempo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );

        btn_esc_novo.setText("Novo");
        btn_esc_novo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_esc_novoActionPerformed(evt);
            }
        });

        btn_esc_excluir.setText("Excluir");
        btn_esc_excluir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_esc_excluirActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 509, Short.MAX_VALUE)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(115, 115, 115)
                .addComponent(btn_esc_novo)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btn_esc_excluir)
                .addGap(118, 118, 118))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 38, Short.MAX_VALUE)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btn_esc_novo)
                    .addComponent(btn_esc_excluir))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        jTabbedPane2.addTab("Escolha", jPanel2);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jTabbedPane2)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jTabbedPane2)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>                        

    private void btn_reg_novoActionPerformed(java.awt.event.ActionEvent evt) {                                             
        ct_reg_codigo.setText("");
        ct_reg_combustivel.setText("");
        ct_reg_tipo.setText("");
        modo = "Novo";
        ManipulaInterface();
         
        JOptionPane.showMessageDialog(null, "Para o bom funcionamento do programa, recomenda-se escrever a palavra com a primeira letra Maiúscula!");

    }                                            

    private void btn_reg_salvarActionPerformed(java.awt.event.ActionEvent evt) {                                               
       int cod = Integer.parseInt(ct_reg_codigo.getText());
       if(modo.equals("Novo"))
       {
        if("Carreta".equals(ct_reg_tipo.getText()))
        {

            Carreta CA = new Carreta();
            CA.setNumeroCadastro(cod);
            CA.setTipo("Carreta");
            CA.setCombustivel("Disel");
            CA.setRendimentoD(8.0f);
            CA.setCargaMax(30000.0f);
            CA.setVelocidadeMedia(60.0f);
            if (ce_reg_status.isSelected())
            {
                CA.setDisponibilidade(true);
            }
            else
            {
                CA.setDisponibilidade(false);
            }
            JOptionPane.showMessageDialog(null,CA.salvar());
            JOptionPane.showMessageDialog(null,CA.salvarLis());
            if(ce_reg_status.isSelected())
            {
                ListaCarreta.add(CA);
            }
            ListaVei.add(CA);
            LoadTableReg();
        }
        
        else if("Van".equals(ct_reg_tipo.getText()))
        {
            
            Van VA = new Van();
            VA.setNumeroCadastro(cod);
            VA.setTipo("Van");
            VA.setCombustivel("Disel");
            VA.setRendimentoD(10.0f);
            VA.setCargaMax(3500.0f);
            VA.setVelocidadeMedia(100.0f);
            if (ce_reg_status.isSelected())
            {
                VA.setDisponibilidade(true);
            }
            else
            {
                VA.setDisponibilidade(false);
            }
            JOptionPane.showMessageDialog(null,VA.salvar());
            JOptionPane.showMessageDialog(null,VA.salvarLis());
            if(ce_reg_status.isSelected())
            {
                ListaVan.add(VA);
            }
            ListaVei.add(VA);
            LoadTableReg();
        }
        
        else if("Carro".equals(ct_reg_tipo.getText()))
        {
            
            Carro CR = new Carro();
            CR.setNumeroCadastro(cod);
            CR.setTipo("Carro");
            CR.setCombustivel(ct_reg_combustivel.getText());
            if("Gasolina".equals(ct_reg_combustivel.getText()))
            {
                CR.setRendimentoG(14.0f);
            }
            else
            {
                CR.setRendimentoA(12.0f);
            }
            CR.setCargaMax(360.0f);
            CR.setVelocidadeMedia(100.0f);
            if (ce_reg_status.isSelected())
            {
                CR.setDisponibilidade(true);
            }
            else
            {
                CR.setDisponibilidade(false);
            }
            JOptionPane.showMessageDialog(null,CR.salvar());
            JOptionPane.showMessageDialog(null,CR.salvarLis());
            if(ce_reg_status.isSelected())
            {
                if("Gasolina".equals(ct_reg_combustivel.getText()))
                {
                    ListaCarroG.add(CR);
                }
                else
                {
                    ListaCarroA.add(CR); 
                }
            }
            ListaCarro.add(CR);
            ListaVei.add(CR);
            LoadTableReg();
        }
        
        else if("Moto".equals(ct_reg_tipo.getText()))
        {
            
            Moto MT = new Moto();
            MT.setNumeroCadastro(cod);
            MT.setTipo("Moto");
            MT.setCombustivel(ct_reg_combustivel.getText());
            if("Gasolina".equals(ct_reg_combustivel.getText()))
            {
                MT.setRendimentoG(50.0f);
            }
            else
            {
                MT.setRendimentoA(43.0f);
            }
            MT.setCargaMax(50.0f);
            MT.setVelocidadeMedia(110.0f);
            if (ce_reg_status.isSelected())
            {
                MT.setDisponibilidade(true);
            }
            else
            {
                MT.setDisponibilidade(false);
            }
            JOptionPane.showMessageDialog(null,MT.salvar());
            JOptionPane.showMessageDialog(null,MT.salvarLis());
            if(ce_reg_status.isSelected())
            {
                if("Gasolina".equals(ct_reg_combustivel.getText()))
                {
                    ListaMotoG.add(MT);
                }
                else
                {
                    ListaMotoA.add(MT); 
                }
            }
            ListaMoto.add(MT);
            ListaVei.add(MT);
            LoadTableReg();
        }
        modo = "Navegar";
        ManipulaInterface();
       }
       else if(modo.equals("Editar"))
       {
           int index = tbl_reg.getSelectedRow();
           ListaVei.get(index).setNumeroCadastro(cod);
           ListaVei.get(index).setTipo(ct_reg_tipo.getText());
           ListaVei.get(index).setCombustivel(ct_reg_combustivel.getText());
           ListaVei.get(index).setDisponibilidade(ce_reg_status.isSelected());
           
           if("Carreta".equals(ct_reg_tipo.getText()))
           {

                Carreta CA = new Carreta();
                CA.setNumeroCadastro(cod);
                CA.setTipo("Carreta");
                CA.setCombustivel("Disel");
                CA.setRendimentoD(8.0f);
                CA.setCargaMax(30000.0f);
                CA.setVelocidadeMedia(60.0f);
                if (ce_reg_status.isSelected())
                {
                    CA.setDisponibilidade(true);
                }
                else
                {
                    CA.setDisponibilidade(false);
                }
                JOptionPane.showMessageDialog(null,CA.salvar());
             
            }
           
            else if("Van".equals(ct_reg_tipo.getText()))
            {
            
                Van VA = new Van();
                VA.setNumeroCadastro(cod);
                VA.setTipo("Van");
                VA.setCombustivel("Disel");
                VA.setRendimentoD(10.0f);
                VA.setCargaMax(3500.0f);
                VA.setVelocidadeMedia(100.0f);
                if (ce_reg_status.isSelected())
                {
                    VA.setDisponibilidade(true);
                }
                else
                {
                    VA.setDisponibilidade(false);
                }
                JOptionPane.showMessageDialog(null,VA.salvar());
            
            }
        
            else if("Carro".equals(ct_reg_tipo.getText()))
            {
            
                Carro CR = new Carro();
                CR.setNumeroCadastro(cod);
                CR.setTipo("Carro");
                CR.setCombustivel(ct_reg_combustivel.getText());
                if("Gasolina".equals(ct_reg_combustivel.getText()))
                {
                    CR.setRendimentoG(14.0f);
                }
                else
                {
                    CR.setRendimentoA(12.0f);
                }
                CR.setCargaMax(360.0f);
                CR.setVelocidadeMedia(100.0f);
                if (ce_reg_status.isSelected())
                {
                    CR.setDisponibilidade(true);
                }
                else
                {
                    CR.setDisponibilidade(false);
                }
                JOptionPane.showMessageDialog(null,CR.salvar());
                
            }
        
            else if("Moto".equals(ct_reg_tipo.getText()))
            {
            
                Moto MT = new Moto();
                MT.setNumeroCadastro(cod);
                MT.setTipo("Moto");
                MT.setCombustivel(ct_reg_combustivel.getText());
                if("Gasolina".equals(ct_reg_combustivel.getText()))
                {
                    MT.setRendimentoG(50.0f);
                }
                else
                {
                    MT.setRendimentoA(43.0f);
                }
                MT.setCargaMax(50.0f);
                MT.setVelocidadeMedia(110.0f);
                if (ce_reg_status.isSelected())
                {
                    MT.setDisponibilidade(true);
                }
                else
                {
                    MT.setDisponibilidade(false);
                }
                JOptionPane.showMessageDialog(null,MT.salvar());
                
            }
           
           LoadTableReg();
           modo = "Navegar";
           ManipulaInterface();
       }
       
    }                                              

    private void btn_reg_editarActionPerformed(java.awt.event.ActionEvent evt) {                                               
        JOptionPane.showMessageDialog(null,"Para o bom funcionamento do programa, não é possível alterar o código do objeto");
        modo = "Editar";
        ManipulaInterface();
    }                                              

    private void ct_reg_codigoActionPerformed(java.awt.event.ActionEvent evt) {                                              
        // TODO add your handling code here:
    }                                             

    private void ct_reg_tipoActionPerformed(java.awt.event.ActionEvent evt) {                                            
        // TODO add your handling code here:
    }                                           

    private void ct_reg_combustivelActionPerformed(java.awt.event.ActionEvent evt) {                                                   
        // TODO add your handling code here:
    }                                                  

    private void ce_reg_statusActionPerformed(java.awt.event.ActionEvent evt) {                                              
        // TODO add your handling code here:
    }                                             

    private void btn_esc_novoActionPerformed(java.awt.event.ActionEvent evt) {                                             
        ct_reg_codigo.setText("");
        ct_reg_combustivel.setText("");
        ct_reg_tipo.setText("");
        modo = "Novo";
        ManipulaInterface();
    }                                            

    private void btn_esc_verificarActionPerformed(java.awt.event.ActionEvent evt) {                                                  
        Pedidos P = new Pedidos();
        float dist = Float.valueOf(ct_esc_distancia.getText());
        float pes = Float.valueOf(ct_esc_peso.getText());
        float temp = Float.valueOf(ct_esc_tempo.getText());
        double cash = Double.valueOf(ct_esc_lucro.getText());
        
        P.setDistancia(dist);
        P.setPeso(pes);
        P.setTempo(temp);
        P.setMargemL(cash);
        String rapido = P.mRapido();
        String valor = P.sLucro();
        double Valor = P.vLucro(valor);
        
        
        if("Erro".equals(P.mRapido()) || "Erro".equals(P.sLucro()))
        {
            JOptionPane.showMessageDialog(null, "Valores Inválidos");
            
            modo = "Navegar";
            ManipulaInterface();
        }
        
        else
        {
            if(janela2 == null)
            {
                janela2 = new JanelaAuxiliar1();
                janela2.setLocationRelativeTo(null);
                janela2.setVisible(true);
                janela2.setResizable(false);
            }
            else
            {
                janela2.setLocationRelativeTo(null);
                janela2.setVisible(true);
                janela2.setResizable(false);

                if("Carreta".equals(rapido))
                {
                    janela2.enviaListatbl1(this, dist, pes, temp, "Carreta", ListaCarro, ListaMoto, ListaCarreta, ListaVan);
                }
                else if("Van".equals(rapido))
                {
                    janela2.enviaListatbl1(this, dist, pes, temp, "Van", ListaCarro, ListaMoto, ListaCarreta, ListaVan);
                }
                else if("Carro".equals(rapido))
                {
                    janela2.enviaListatbl1(this, dist, pes, temp, "Carro", ListaCarro, ListaMoto, ListaCarreta, ListaVan);
                }
                else if("Moto".equals(rapido))
                {
                    janela2.enviaListatbl1(this, dist, pes, temp, "Moto", ListaCarro, ListaMoto, ListaCarreta, ListaVan);
                }
                
                if("Carreta".equals(valor))
                {
                    janela2.enviaListatbl2(this, dist, pes, temp, "Carreta", ListaCarro, ListaMoto, ListaCarreta, ListaVan);
                }
                else if("Van".equals(valor))
                {
                    janela2.enviaListatbl2(this, dist, pes, temp, "Van", ListaCarro, ListaMoto, ListaCarreta, ListaVan);
                }
                else if("CarroG".equals(valor))
                {
                    janela2.enviaListatbl2(this, dist, pes, temp, "CarroG", ListaCarroG, ListaMoto, ListaCarreta, ListaVan);
                }
                else if("CarroA".equals(valor))
                {
                    janela2.enviaListatbl2(this, dist, pes, temp, "CarroA", ListaCarroA, ListaMoto, ListaCarreta, ListaVan);
                }
                else if("MotoG".equals(valor))
                {
                    janela2.enviaListatbl1(this, dist, pes, temp, "MotoG", ListaCarro, ListaMotoG, ListaCarreta, ListaVan);
                }
                else if("MotoA".equals(valor))
                {
                    janela2.enviaListatbl2(this, dist, pes, temp, "MotoA", ListaCarroG, ListaMotoA, ListaCarreta, ListaVan);
                }
     
        }
       modo = "Navegar";
       ManipulaInterface();
    }                                                 

    private void btn_reg_cancelarActionPerformed(java.awt.event.ActionEvent evt) {                                                 
        ct_reg_codigo.setText("");
        ct_reg_combustivel.setText("");
        ct_reg_tipo.setText("");
        modo = "Navegar";
        ManipulaInterface();
    }                                                

    private void btn_reg_excluirActionPerformed(java.awt.event.ActionEvent evt) {                                                
        int index = tbl_reg.getSelectedRow();
        
        if(index>=0 && index<ListaVei.size())
        { 
            ListaVei.remove(index);
        }
        ExcluirArq(String.valueOf(ct_reg_codigo.getText()));
        LoadTableReg();
        modo = "Navegar";
        ManipulaInterface();
    }                                               

    private void tbl_regMouseClicked(java.awt.event.MouseEvent evt) {                                     
        int index = tbl_reg.getSelectedRow();
        
        if(index>=0 && index<ListaVei.size())
        { 
            Veiculos v = ListaVei.get(index);
            
            ct_reg_codigo.setText(String.valueOf(v.getNumeroCadastro()));
            ct_reg_tipo.setText(v.getTipo());
            ct_reg_combustivel.setText(v.getCombustivel());
            modo = "Selecao";
            ManipulaInterface();

        }
    }                                    

    private void ct_esc_distanciaActionPerformed(java.awt.event.ActionEvent evt) {                                                 
        // TODO add your handling code here:
    }                                                

    private void btn_esc_excluirActionPerformed(java.awt.event.ActionEvent evt) {                                                
        int index = tbl_esc.getSelectedRow();
        
        if(index>=0 && index<ListaPed.size())
        { 
            ListaPed.remove(index);
        }
        ExcluirArqP(String.valueOf(ListaPed.get(index).getNumeroPedido()));
        LoadTablePed();
        modo = "Navegar";
        ManipulaInterface();
    }                                               

    private void tbl_escMouseClicked(java.awt.event.MouseEvent evt) {                                     

    }                                    

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Main.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Main.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Main.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Main.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Main().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify                     
    private javax.swing.JButton btn_esc_excluir;
    private javax.swing.JButton btn_esc_novo;
    private javax.swing.JButton btn_esc_verificar;
    private javax.swing.JButton btn_reg_cancelar;
    private javax.swing.JButton btn_reg_editar;
    private javax.swing.JToggleButton btn_reg_excluir;
    private javax.swing.JButton btn_reg_novo;
    private javax.swing.JButton btn_reg_salvar;
    private javax.swing.JCheckBox ce_reg_status;
    private javax.swing.JTextField ct_esc_distancia;
    private javax.swing.JTextField ct_esc_lucro;
    private javax.swing.JTextField ct_esc_peso;
    private javax.swing.JTextField ct_esc_tempo;
    private javax.swing.JTextField ct_reg_codigo;
    private javax.swing.JTextField ct_reg_combustivel;
    private javax.swing.JTextField ct_reg_tipo;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JTabbedPane jTabbedPane2;
    private javax.swing.JTable tbl_esc;
    private javax.swing.JTable tbl_reg;
    // End of variables declaration                   
}
